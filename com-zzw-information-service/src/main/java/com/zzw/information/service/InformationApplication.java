package com.zzw.information.service;

import org.springframework.boot.SpringApplication;

public class InformationApplication {
	public static void main(String[] args) {
		SpringApplication.run(InformationApplication.class, args);
	}
}
