package com.zzw.device.service;

import org.springframework.boot.SpringApplication;

public class DeviceApplication {
	public static void main(String[] args) {
		SpringApplication.run(DeviceApplication.class, args);
	}
}
