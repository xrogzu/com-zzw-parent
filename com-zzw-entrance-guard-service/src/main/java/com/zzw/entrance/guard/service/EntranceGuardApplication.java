package com.zzw.entrance.guard.service;

import org.springframework.boot.SpringApplication;

public class EntranceGuardApplication {
	public static void main(String[] args) {
		SpringApplication.run(EntranceGuardApplication.class, args);
	}
}
